Name:           qt5-ukui-platformtheme
Version:        4.0.0.0
Release:        1
Summary:        Qt5 QPA platform theme of UKUI
License:        LGPL-3.0-or-later and GPL-3.0-or-later
URL:            http://www.ukui.org
Source0:        %{name}-%{version}.tar.gz


BuildRequires: dbus-x11
BuildRequires: kf5-kirigami2-devel
BuildRequires: fontconfig-devel
BuildRequires: freetype-devel
BuildRequires: glib2-devel
BuildRequires: gsettings-qt-devel
BuildRequires: kf5-kiconthemes-devel
BuildRequires: kf5-kwayland-devel
BuildRequires: kf5-kwindowsystem-devel
BuildRequires: libkysdk-waylandhelper-devel
BuildRequires: libpeony-devel
BuildRequires: qt5-qtsvg-devel
BuildRequires: qt5-qtx11extras-devel
BuildRequires: libqtxdg-devel
BuildRequires: peony
BuildRequires: pkg-config
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-devel
BuildRequires: qt5-qtbase-private-devel
BuildRequires: qtchooser
BuildRequires: qt5-qtdeclarative
BuildRequires: qt5-qtquickcontrols2-devel
BuildRequires: qt5-qttools-devel
BuildRequires: kf5-kconfig-devel
BuildRequires: qt5-qtbase-static


Requires: libqt5-ukui-style1
Requires: qt5-styles-ukui
Requires: qml-module-org-ukui-qqc2desktopstyle
Requires: qml-module-org-ukui-stylehelper

%description
 qt5-ukui-platformtheme is official platform theme of UKUI desktop
 environment. It also provides the common metadatas for ukui-styles
 and platform theme using. The library provided many convenient API
 for changing a qt widgets style, such as buttons color, tabwidget
 animation, etc.
 .
 This package provides a qt5 qpa platform theme plugin.

%package -n qt5-styles-ukui
Summary:     QStyle plugins provided by ukui
License:     LGPLv2+
Requires: libqt5-ukui-style1

%description -n qt5-styles-ukui
 qt5-ukui-platformtheme is official platform theme of UKUI desktop
 environment. It also provides the common metadatas for ukui-styles
 and platform theme using. The library provided many convenient API
 for changing a qt widgets style, such as buttons color, tabwidget
 animation, etc.
 .
 This package provides several qstyle plugins which as default
 styles in ukui. For now, fusion is the base style of ukui-styles.


%package -n libqt5-ukui-style1
Summary:  UKUI platform theme and styles' shared library
License:  LGPLv2+
Requires: glib2,  qt5-qtbase-gui, gsettings-qt
Provides: libqt5-ukui-style

%description -n libqt5-ukui-style1
 qt5-ukui-platformtheme is official platform theme of UKUI desktop
 environment. It also provides the common metadatas for ukui-styles
 and platform theme using. The library provided many convenient API
 for changing a qt widgets style, such as buttons color, tabwidget
 animation, etc.
 .
 This package provides the shared libraries used by ukui platform
 theme and ukui-styles.

%package -n libqt5-ukui-style-dev
Summary:  Development files of libqt5-ukui-style1
License:  LGPLv2+
Requires: libqt5-ukui-style1 = %{version}

%description -n libqt5-ukui-style-dev
 qt5-ukui-platformtheme is official platform theme of UKUI desktop
 environment. It also provides the common metadatas for ukui-styles
 and platform theme using. The library provided many convenient API
 for changing a qt widgets style, such as buttons color, tabwidget
 animation, etc.
 .
 This package provides the development files of libqt5-ukui-style1.

%package -n qml-module-org-ukui-qqc2desktopstyle
Summary:  Qt Quick Controls 2: Kylin Style
License:  LGPLv2+
Requires: qml-module-org-ukui-stylehelper qt5-qtgraphicaleffects qt5-qtdeclarative qt5-qtquickcontrols2 qt5-qtdeclarative qt5-qtquickcontrols2

%description -n qml-module-org-ukui-qqc2desktopstyle
 Small style written in QML for QtQuickControls2 intended to be used
 by default in QQC2-based apps when used in the Kylin tablet desktop

%package -n qml-module-org-ukui-stylehelper
Summary:  qml stylehelper
License:  LGPLv2+
Requires: qt5-qtgraphicaleffects qt5-qtdeclarative qt5-qtquickcontrols2 qt5-qtdeclarative qt5-qtquickcontrols2

%description -n qml-module-org-ukui-stylehelper
 The external dependency Lib of qml theme framework



%prep
%setup -q

%build
mkdir build && cd build
%{qmake_qt5} ..
%{make_build}

%post
glib-compile-schemas /usr/share/glib-2.0/schemas &> /dev/null ||:

%install
rm -rf $RPM_BUILD_ROOT

cd %{_builddir}/%{name}-%{version}/build
%{make_install} INSTALL_ROOT=%{buildroot}

mkdir -p %{buildroot}/usr/include/qt5-ukui/
cp -rf %{_builddir}/%{name}-%{version}/libqt5-ukui-style/*/*.h %{buildroot}/usr/include/qt5-ukui/
cp -rf %{_builddir}/%{name}-%{version}/libqt5-ukui-style/*/*/*.h %{buildroot}/usr/include/qt5-ukui/

%clean
rm -rf $RPM_BUILD_ROOT

%files
%doc debian/changelog  debian/copyright
%{_libdir}/qt5/plugins/platformthemes/*.so
%{_datadir}/qt5-ukui-platformtheme/*
%exclude /opt/filedialog/bin/filedialog
%exclude /opt/messagebox/bin/messagebox

%files -n qt5-styles-ukui
%{_libdir}/qt5/plugins/styles/*.so

%files -n libqt5-ukui-style1
%{_libdir}/*.so.*
%{_datadir}/glib-2.0/schemas/org.ukui.style.gschema.xml

%files -n libqt5-ukui-style-dev
%{_includedir}/qt5-ukui/*
%{_libdir}/pkgconfig/*.pc
%{_libdir}/*.so

%files -n qml-module-org-ukui-qqc2desktopstyle
%{_libdir}/qt5/qml/QtQuick/Controls.2/*

%files -n qml-module-org-ukui-stylehelper
%{_libdir}/qt5/qml/org/ukui/qqc2style/*


%changelog
* Thu Mar 28 2024 huayadong <huayadong@kylinos.cn> - 4.0.0.0-1
- update version to 4.0.0.0

* Wed Feb 08 2023 peijiankang <peijiankang@kylinos.cn> - 3.1.4-2
- add build debuginfo and debugsource

* Tue Dec 27 2022 lvfei<lvfei@kylinos.cn> - 3.1.4-1
- update upstream version 3.1.4

* Thu Dec 15 2022 tanyulong<tanyulong@kylinos.cn> - 3.1.3-2
- Improve the project according to the requirements of compliance improvement

* Mon Nov 21 2022 tanyulong <tanyulong@kylinos.cn> - 3.1.3-1
- update upstream version 3.1.3

* Tue Jul 12 2022 peijiankang <peijiankang@kylinos.cn> - 1.0.8-4
- update version to 1.0.8

* Thu May 19 2022 tanyulong<tanyulong@kylinos.cn> - 1.0.5-7
- Improve the project according to the requirements of compliance improvement

* Tue Apr 19 2022 pei-jiankang <peijiankang@kylinos.cn> - 1.0.5-6
- modify qt5-ukui-platformtheme install error 

* Mon Nov 08 2021 tanyulong <tanyulong@kylinos.cn> - 1.0.5-5
- Modify the disable status font alpha

* Fri Nov 05 2021 tanyulong <tanyulong@kylinos.cn> - 1.0.5-4
- Modify the default themename is ukui

* Tue Nov 02 2021 tanyulong <tanyulong@kylinos.cn> - 1.0.5-3
- add patch1: 0002-Fix-return-type-errors.patch

* Thu Oct 21 2021 douyan<douyan@kylinos.cn> - 1.0.5-2
- add patch0: 0001-change-libqt5-ukui-style-Requires-Qt5Widgets-Version.patch

* Mon Oct 26 2020 douyan <douyan@kylinos.cn> - 1.0.5-1
- update to upstream version 1.0.4-1+1027.1

* Thu Jul 9 2020 douyan <douyan@kylinos.cn> - 1.0.3-1
- Init package for openEuler
